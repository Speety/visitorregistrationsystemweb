﻿using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO;

namespace VisitorRegistrationSystem.BLL.Interfaces
{
    public interface IEmployeeService
    {
        void AddEmployee(EmployeeDTO employeeDto);
        void DeleteEmployee(int? id);
        EmployeeDTO GetEmployee(int? id);
        IEnumerable<EmployeeDTO> GetEmployees();
        void EditEmployee(EmployeeDTO employeeDto);
        void Dispose();
    }
}
