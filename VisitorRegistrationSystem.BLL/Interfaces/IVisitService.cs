﻿using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO;

namespace VisitorRegistrationSystem.BLL.Interfaces
{
    public interface IVisitService
    {
        void AddVisit(VisitDTO visitDto);
        void DeleteVisit(int? id);
        VisitDTO GetVisit(int? id);
        IEnumerable<VisitDTO> GetVisits();
        void EditVisit(VisitDTO visitDto);
        VisitDTO GetVisitByNumber(string visitNumber);
        IEnumerable<VisitDTO> GetVisitsFromDate(string startDate, string endDate);
        void Dispose();
    }
}
