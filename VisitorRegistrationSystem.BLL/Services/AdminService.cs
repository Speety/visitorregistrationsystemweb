﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using VisitorRegistrationSystem.BLL.DTO;
using VisitorRegistrationSystem.BLL.Infrastructure;
using VisitorRegistrationSystem.BLL.Interfaces;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;

namespace VisitorRegistrationSystem.BLL.Services
{
    public class AdminService : IAdminService
    {
        IAdminRepository adminRepository { get; set; }

        public AdminService(IAdminRepository rep)
        {
            //// config AutoMapper
            //Mapper.Initialize(cfg => {
            //    cfg.CreateMap<ApplicationDTO, Application>();
            //    cfg.CreateMap<Application, ApplicationDTO>();
            //});

            this.adminRepository = rep;
        }

        public void AddAdmin(AdminDTO adminDto)
        {
            // validate
            if (adminDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // mapping
                var admin = Mapper.Map<AdminDTO, Admin>(adminDto);
                this.adminRepository.Admins.Add(admin);
                this.adminRepository.Admins.Save();
            }
        }

        public IEnumerable<AdminDTO> GetAdmins()
        {
            // mapping
            return Mapper.Map<IEnumerable<Admin>, List<AdminDTO>>(this.adminRepository.Admins.GetAll());
        }

        public AdminDTO GetAdmin(int? id)
        {
            if (id != null)
            {
                var admin = this.adminRepository.Admins.GetById(id.Value);
                if (admin == null)
                    throw new MapperValidationException("application not found", "");

                // mapping
                return Mapper.Map<Admin, AdminDTO>(admin);
            }

            throw new MapperValidationException("id not found", "");
        }

        public void DeleteAdmin(int? id)
        {
            // validation
            if (id == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                var admin = adminRepository.Admins.GetById(id.Value);
                this.adminRepository.Admins.Delete(admin);
                this.adminRepository.Admins.Save();
            }
        }

        public void EditAdmin(AdminDTO adminDto)
        {
            // validation
            if (adminDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // process mapping
                var adminToEdit = Mapper.Map<AdminDTO, Admin>(adminDto);
                this.adminRepository.Admins.Edit(adminToEdit);
                this.adminRepository.Admins.Save();
            }
        }

        public AdminDTO GetByLogin(string login)
        {
            return Mapper.Map<Admin, AdminDTO>(this.adminRepository.Admins.GetAll().FirstOrDefault(a => a.Login == login));
        }

        public void Dispose()
        {
            this.adminRepository.Dispose();
        }
    }
}
