﻿using AutoMapper;
using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO;
using VisitorRegistrationSystem.BLL.Infrastructure;
using VisitorRegistrationSystem.BLL.Interfaces;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;

namespace VisitorRegistrationSystem.BLL.Services
{
    public class EmployeeService : IEmployeeService
    {
        IEmployeeRepository employeeRepository { get; set; }

        public EmployeeService(IEmployeeRepository rep)
        {
            //// Настройка AutoMapper
            //Mapper.Initialize(cfg => {
            //    cfg.CreateMap<ApplicationDTO, Application>();
            //    cfg.CreateMap<Application, ApplicationDTO>();
            //});

            employeeRepository = rep;
        }

        public void AddEmployee(EmployeeDTO employeeDto)
        {
            // validate
            if (employeeDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // mapping
                var employee = Mapper.Map<EmployeeDTO, Employee>(employeeDto);
                this.employeeRepository.Employees.Add(employee);
                this.employeeRepository.Employees.Save();
            }
        }

        public IEnumerable<EmployeeDTO> GetEmployees()
        {
            // mapping
            return Mapper.Map<IEnumerable<Employee>, List<EmployeeDTO>>(this.employeeRepository.Employees.GetAll());
        }

        public EmployeeDTO GetEmployee(int? id)
        {
            if (id != null)
            {
                var employee = this.employeeRepository.Employees.GetById(id.Value);
                if (employee == null)
                    throw new MapperValidationException("application not found", "");

                // mapping
                return Mapper.Map<Employee, EmployeeDTO>(employee);
            }

            throw new MapperValidationException("id not found", "");
        }

        public void DeleteEmployee(int? id)
        {
            // validation
            if (id == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                var employee = employeeRepository.Employees.GetById(id.Value);
                this.employeeRepository.Employees.Delete(employee);
                this.employeeRepository.Employees.Save();
            }
        }

        public void EditEmployee(EmployeeDTO employeeDto)
        {
            // validation
            if (employeeDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // process mapping
                var employeeToEdit = Mapper.Map<EmployeeDTO, Employee>(employeeDto);
                this.employeeRepository.Employees.Edit(employeeToEdit);
                this.employeeRepository.Employees.Save();
            }
        }

        public void Dispose()
        {
            this.employeeRepository.Dispose();
        }
    }
}
