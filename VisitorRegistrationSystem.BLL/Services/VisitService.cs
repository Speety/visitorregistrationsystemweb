﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VisitorRegistrationSystem.BLL.DTO;
using VisitorRegistrationSystem.BLL.Infrastructure;
using VisitorRegistrationSystem.BLL.Interfaces;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;

namespace VisitorRegistrationSystem.BLL.Services
{
    public class VisitService : IVisitService
    {
        IVisitRepository visitRepository { get; set; }

        public VisitService(IVisitRepository rep)
        {
            //// Настройка AutoMapper
            //Mapper.Initialize(cfg => {
            //    cfg.CreateMap<ApplicationDTO, Application>();
            //    cfg.CreateMap<Application, ApplicationDTO>();
            //});

            visitRepository = rep;
        }

        public void AddVisit(VisitDTO visitDto)
        {
            // validate
            if (visitDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // mapping
                this.visitRepository.Visits.Add(Mapper.Map<VisitDTO, Visit>(visitDto));
                this.visitRepository.Visits.Save();
            }
        }

        public VisitDTO GetVisitByNumber(string visitNumber)
        {
            if (!string.IsNullOrEmpty(visitNumber))
            {
                var visit = this.visitRepository.Visits.GetAll().FirstOrDefault(a => a.Number == visitNumber);
                if (visit == null)
                    throw new MapperValidationException("application not found", "");

                // mapping
                return Mapper.Map<Visit, VisitDTO>(visit);
            }

            throw new MapperValidationException("visitNumber not found", "");
        }


        public IEnumerable<VisitDTO> GetVisits()
        {
            // mapping
            return Mapper.Map<IEnumerable<Visit>, List<VisitDTO>>(this.visitRepository.Visits.GetAll());
        }

        public VisitDTO GetVisit(int? id)
        {
            if (id != null)
            {
                var visit = this.visitRepository.Visits.GetById(id.Value);
                if (visit == null)
                    throw new MapperValidationException("application not found", "");

                // mapping
                return Mapper.Map<Visit, VisitDTO>(visit);
            }

            throw new MapperValidationException("id not found", "");
        }

        public void DeleteVisit(int? id)
        {
            // validation
            if (id == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                var visit = visitRepository.Visits.GetById(id.Value);
                this.visitRepository.Visits.Delete(visit);
                this.visitRepository.Visits.Save();
            }
        }

        public void EditVisit(VisitDTO visitDto)
        {
            // validation
            if (visitDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // process mapping
                var visitToEdit = Mapper.Map<VisitDTO, Visit>(visitDto);
                this.visitRepository.Visits.Edit(visitToEdit);
                this.visitRepository.Visits.Save();
            }
        }

        public IEnumerable<VisitDTO> GetVisitsFromDate(string startDate, string endDate)
        {
            try
            {
                // validation
                if (string.IsNullOrEmpty(startDate) && string.IsNullOrEmpty(endDate))
                {
                    throw new MapperValidationException("application can't be added because it's null", "");
                }
                else
                {
                    DateTime startDateTime;
                    DateTime endDateTime;
                    DateTime.TryParse(startDate, out startDateTime);
                    DateTime.TryParse(endDate, out endDateTime);


                    var list = this.visitRepository.Visits.GetAll().ToList().Where(x => Convert.ToDateTime(x.CheckInTime) >= startDateTime && Convert.ToDateTime(x.CheckInTime) <= endDateTime);
                    return Mapper.Map<IEnumerable<Visit>, List<VisitDTO>>(list);
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void Dispose()
        {
            this.visitRepository.Dispose();
        }
    }
}
