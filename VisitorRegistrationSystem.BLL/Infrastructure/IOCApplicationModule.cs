﻿using Ninject.Modules;
using VisitorRegistrationSystem.BLL.Interfaces;
using VisitorRegistrationSystem.BLL.Services;

namespace VisitorRegistrationSystem.BLL.Infrastructure
{
    public class IOCApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAdminService>().To<AdminService>();
            Bind<IVisitService>().To<VisitService>();
            Bind<IEmployeeService>().To<EmployeeService>();
            Bind<IVisitorService>().To<VisitorService>();
        }
    }
}
