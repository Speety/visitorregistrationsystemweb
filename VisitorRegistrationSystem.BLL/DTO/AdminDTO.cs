﻿using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;
using VisitorRegistrationSystem.DAL.Enums;

namespace VisitorRegistrationSystem.BLL.DTO
{
    public class AdminDTO : EntityBase
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public AccessLevelEnum AccessLevel { get; set; }
    }
}
