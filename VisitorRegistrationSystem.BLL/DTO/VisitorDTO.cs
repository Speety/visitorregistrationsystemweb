﻿using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO.BaseDTO;
using VisitorRegistrationSystem.DAL.Enteties;

namespace VisitorRegistrationSystem.BLL.DTO
{
    public class VisitorDTO : PersonDTO
    {
        public string CheckInTime { get; set; }

        public string CheckOutTime { get; set; }

        public string BadgeNumber { get; set; }

        public string CompanyName { get; set; }

        public virtual List<Visit> Visits { get; set; }
    }
}
