﻿using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO.BaseDTO;
using VisitorRegistrationSystem.DAL.Enteties;

namespace VisitorRegistrationSystem.BLL.DTO
{
    public class EmployeeDTO : PersonDTO
    {
        public string Email { get; set; }

        public string Position { get; set; }

        public string PhoneNumber { get; set; }

        public string PhotoPath { get; set; }

        public virtual List<Visit> Visits { get; set; }
    }
}
