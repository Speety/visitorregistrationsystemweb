﻿using System.Data.Entity;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces;

namespace VisitorRegistrationSystem.DAL.Data
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext(string connectionString) : base(connectionString)
        {
            //Database.SetInitializer(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>().ToTable("Admins").HasKey(q => q.Id);
            modelBuilder.Entity<Visit>().ToTable("Visits").HasKey(q => q.Id);
            modelBuilder.Entity<Employee>().ToTable("Employees").HasKey(q => q.Id);
            modelBuilder.Entity<Visitor>().ToTable("Visitors").HasKey(q => q.Id);


            // configures one-to-many relationship
            modelBuilder.Entity<Visit>()
                .HasRequired<Employee>(s => s.Employee)
                .WithMany(g => g.Visits)
                .HasForeignKey<int>(s => s.EmployeeId);

            // configures one-to-many relationship
            modelBuilder.Entity<Visit>()
                .HasRequired<Visitor>(s => s.Visitor)
                .WithMany(g => g.Visits)
                .HasForeignKey<int>(s => s.VisitorId);
        }

        public System.Data.Entity.DbSet<Admin> Admins { get; set; }
        public System.Data.Entity.DbSet<Visit> Appointments { get; set; }
        public System.Data.Entity.DbSet<Employee> Employees { get; set; }
        public System.Data.Entity.DbSet<Visitor> Visitors { get; set; }
    }
}
