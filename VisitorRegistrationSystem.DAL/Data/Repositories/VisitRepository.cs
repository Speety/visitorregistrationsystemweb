﻿using System;
using VisitorRegistrationSystem.DAL.Data.Repositories.Base;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories.Base;

namespace VisitorRegistrationSystem.DAL.Data.Repositories
{
    public class VisitRepository : IVisitRepository
    {
        private DataContext dbContext;
        private IRepository<Visit> visitRepository;

        public VisitRepository(string connectionString)
        {
            dbContext = new DataContext(connectionString);
        }

        public IRepository<Visit> Visits
        {
            get
            {
                if (visitRepository == null)
                    visitRepository = new Repository<Visit>(dbContext);
                return visitRepository;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
