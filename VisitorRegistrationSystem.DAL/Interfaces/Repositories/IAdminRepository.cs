﻿using System;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories.Base;

namespace VisitorRegistrationSystem.DAL.Interfaces.Repositories
{
    public interface IAdminRepository : IDisposable
    {
        IRepository<Admin> Admins { get; }
    }
}
