﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;
using VisitorRegistrationSystem.DAL.Enums;

namespace VisitorRegistrationSystem.DAL.Enteties
{
    public class Admin : EntityBase
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public AccessLevelEnum AccessLevel { get; set; }
    }
}
