﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegistrationSystem.DAL.Enteties
{
    public class Visit : EntityBase
    {
        [Required]
        public string Number { get; set; }

        [Required]
        public string CheckInTime { get; set; }

        public int VisitorId { get; set; }

        public int EmployeeId { get; set; }

        public virtual Visitor Visitor { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
