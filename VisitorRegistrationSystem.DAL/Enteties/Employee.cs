﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base;

namespace VisitorRegistrationSystem.DAL.Enteties
{
    public class Employee : Person
    {
        public Employee()
        {
            Visits = new Collection<Visit>();
        }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Position { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public string PhotoPath { get; set; }

        public virtual ICollection<Visit> Visits { get; set; }
    }
}
