﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base;

namespace VisitorRegistrationSystem.DAL.Enteties
{
    public class Visitor : Person
    {
        public Visitor()
        {
            Visits = new Collection<Visit>();
        }

        [Required]
        public string CheckInTime { get; set; }

        public string CheckOutTime { get; set; }

        [Required]
        public string BadgeNumber { get; set; }

        public string CompanyName { get; set; }

        public virtual ICollection<Visit> Visits { get; set; }
    }
}
