﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegisrationSystemServer.Models
{
    public class VisitViewModelPanel : EntityBase
    {
        [Display(Name = "Visit number")]
        public string Number { get; set; }

        [Display(Name = "Visitor name")]
        public string VisitorName { get; set; }

        [Display(Name = "Visitor badge number")]
        public string VisitorBadge { get; set; }

        [Display(Name = "Employee name")]
        public string EmployeeName { get; set; }

        [Display(Name = "Checkin time")]
        public string CheckInTime { get; set; }

        [Display(Name = "Checkout time")]
        public string CheckOutTime { get; set; }
    }
}