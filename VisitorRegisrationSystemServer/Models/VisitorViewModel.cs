﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegisrationSystemServer.Models
{
    public class VisitorViewModel : EntityBase
    {
        [Required(ErrorMessage = "Please provide the name of visitor")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only allowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        [Display(Name = "First Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please provide the last name of visitor")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only allowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please provide the time of visit")]
        [RegularExpression(@"^(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])/[2][0][1-3][0-9] ([0-9]|0[0-9]|1[0-2]):[0-5][0-9] ([AaPp][Mm])$", ErrorMessage = "Wrong formatting")]
        [Display(Name = "Checkin Time")]
        public string CheckInTime { get; set; }

        [RegularExpression(@"^(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])/[2][0][1-3][0-9] ([0-9]|0[0-9]|1[0-2]):[0-5][0-9] ([AaPp][Mm])$", ErrorMessage = "Wrong formatting")]
        [Display(Name = "Checkout Time")]
        public string CheckOutTime { get; set; }

        [Required(ErrorMessage = "Please provide the time of visit")]
        [RegularExpression(@"^[a-zA-Z0-9_.-]*$", ErrorMessage = "Letters and numbers are only allowed")]
        [StringLength(5, ErrorMessage = "Number length should be not more then 5 characters")]
        [MinLength(5, ErrorMessage = "Number minimum length is 5 characters")]
        [Display(Name = "Badge Number")]
        public string BadgeNumber { get; set; }

        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z0-9_.-]*$", ErrorMessage = "Letters and numbers are only allowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
    }
}