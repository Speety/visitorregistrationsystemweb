﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;
using VisitorRegistrationSystem.DAL.Enums;

namespace VisitorRegisrationSystemServer.Models
{
    public class AdminViewModel : EntityBase
    {
        [Required(ErrorMessage = "Please enter Login")]
        [StringLength(50, ErrorMessage = "Login length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z0-9_.-]*$", ErrorMessage = "Letters or numbers are only anllowed")]
        //[MinLength(6, ErrorMessage = "Login minimum length is 6 characters")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [StringLength(50, ErrorMessage = "Login length should be not more then 50 characters")]
        //[MinLength(6, ErrorMessage = "Password minimum length is 6 characters")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please select the Role")]
        [Display(Name = "Role")]
        public AccessLevelEnum AccessLevel { get; set; }

        public IEnumerable<SelectListItem> AccessLevelItems()
        {
            yield return new SelectListItem { Text = "Admin", Value = "Admin" };
            yield return new SelectListItem { Text = "Moderator", Value = "Moderator" };
        }
    }
}