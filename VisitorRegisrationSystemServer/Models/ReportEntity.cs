﻿using System.ComponentModel.DataAnnotations;

namespace VisitorRegisrationSystemServer.Models
{
    public class ReportEntity
    {
        [Required(ErrorMessage = "Please provide the start date")]
        [RegularExpression(@"^(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])/[2][0][1-3][0-9] ([0-9]|0[0-9]|1[0-2]):[0-5][0-9] ([AaPp][Mm])$", ErrorMessage = "Wrong formatting")]
        [Display(Name = "Start Date")]
        public string StartDate { get; set; }

        [Required(ErrorMessage = "Please provide the end date")]
        [RegularExpression(@"^(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])/[2][0][1-3][0-9] ([0-9]|0[0-9]|1[0-2]):[0-5][0-9] ([AaPp][Mm])$", ErrorMessage = "Wrong formatting")]
        [Display(Name = "End Date")]
        public string EndDate { get; set; }
    }
}