﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace VisitorRegisrationSystemServer
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
            name: "ActionRoute",
            routeTemplate: "api/{controller}/{action}"
        );

            //config.Filters.Add(new RequireHttpsAttribute());
        }
    }
}
